import requests

API = "0d67ba35abe0d4b41e1d64efc5b24dca"
# SESSION_ID = "20ac4c1d56ec4d73a28ed4d59f9dcfc5ef8dd888"
# https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=en-US&page=1&include_adult=false

def find_movie_id(movie_title):
    URL = "https://api.themoviedb.org/3/search/movie?api_key=" + API + "&query=" + movie_title
    response = requests.get(URL)
    json = response.json()
    results = json["results"]
    first_movie = results[0]
    movie_id = first_movie["id"]
    return movie_id

def rate_movie(movie_id, rating, session_id):
    URL = "https://api.themoviedb.org/3/movie/" + str(movie_id) + "/rating?api_key=" + API + "&session_id=" + session_id
    #                                                              /rating?api_key=123session_id=abc

    response = requests.post(URL, json={"value": rating})
    status_code = response.status_code
    assert status_code == 201

def rate_movie_by_title(movie_title, rating, session_id):
    movie_id = find_movie_id(movie_title)
    rate_movie(movie_id, rating, session_id)
