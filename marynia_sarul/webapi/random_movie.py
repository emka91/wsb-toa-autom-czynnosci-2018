
import requests
import random
from mail_sender import send_email

# bliblioteka potrzebna do tego by wysłać maila z losowym filmem


# do zmiennej API przypisz ciag znakow z moim API key
API = "0d67ba35abe0d4b41e1d64efc5b24dca"
# do zmiennej Actor przypisz ciag znakow Edward Norton
# Actor = "Edward Norton"


# zdefiniuj funkcje get_actor_id ktora bedzie przyjmowala jeden parametr o nazwe actor_name
def get_actor_id(actor_name):
    # sklej URL z kilku elementow ( gdzie mam wysłać żądanie )
    URL = "https://api.themoviedb.org/3/search/person?api_key=" + API + "&language=en-US&query=" + actor_name + "&page=1&include_adult=false"

    # stworz zmienna response i uzywajac biblioteki requests i jej funkcji get, wykonaj HTTP reqests GET pod podany URL
    response = requests.get(URL)

    # zmienna response to teraz odpowiedz na mojego requesta (http)

    # z odpowiedzi (response) wyciagnij jego cialo (body / content) za pomoca funkcji json() i przypisz jej wynik do zmiennej json
    # dlatego używam funkcji json bo wiem z dokumentacji ze będzie to json

    json = response.json()
    # z tej zmiennej json ktora zawiera naszego jsona, wyciagnij element o nazwie "results"(liste wynikow)

    results = json["results"]

    # wiemy z dokumentacji ze w jsonie "results" zawiera liste wszystkich osob a nas interesuje tylko pierwsza
    # wyciagamy z tych wszystkich rsults tylko pierwszy element - pierwszy czyli ten o indexie 0
    # indeksowanie jest od 0 więc bierzemy zero

    if len(results) == 0:
        return None
    else:
        single_result = results[0]

        # mamy juz konkretny szukany element i z niego wyciagamy jego pole id

        id = single_result["id"]
        # zwracamy to id jako wynik naszej funkcji
        return id

# uruchamamy zdefniowana wyzj funkcje przekazujac jako parametr wczesniej zdefiniowana zmienna Actor ktoa zawiera tekst "Edward Norton"
# wynik uruchomienia tej funkcji to id tej osoby i przypisujemy go do nowej zmiennej ed_id
#ed_id = get_actor_id(Actor)

# jak wyzej tylko teraz uruchamiamy nasza funkcje przekazujac inny tekst i nie w postaci zmiennej tylko po prostu jako tekst
#harris_id = get_actor_id("Ed Harris")

# wypisze na ekranie id tych aktorow - mamy to juz w zmiennych

#print(ed_id)

def get_random_movie(actor_id):

    URL = "https://api.themoviedb.org/3/person/" + str(actor_id) + "/movie_credits?api_key=" + API + "&language=en-US"
    response = requests.get(URL)
    json = response.json()
    cast_list = json["cast"]
    min_index = 0
    max_index = len(cast_list) - 1
    random_index = random.randrange(min_index, max_index)
    random_movie = cast_list[random_index]
    movie_title = random_movie["title"]
    movie_description = random_movie["overview"]
    movie_link = "https://themoviedb.org/movie/" + str(random_movie["id"])
    return movie_title, movie_description, movie_link





def get_actors_random_movie(actor_name):
    actor_id = get_actor_id(actor_name)
    if actor_id is None:
        print("actor not found")
    else:
        movie_title, movie_description, movie_link = get_random_movie(actor_id)
        print(movie_title)
        return movie_title, movie_description, movie_link


def send_random_movie(actor_name, email):
    movie_title, movie_description, movie_link = get_actors_random_movie(actor_name)
    send_email(email, movie_title, movie_description, movie_link)
