import time
import random
from selenium.webdriver.common.action_chains import ActionChains


class MainPage:

    def __init__(self, driver):
        self.driver = driver
        assert "TMDb" in driver.title

    def click_cookie_notice(self):
        close_button = self.driver.find_element_by_css_selector("div#cookie_notice > p > a.accept")
        if close_button is not None:
            close_button.click()

    def search_for_actor(self, actor_name):
        search_box = self.driver.find_element_by_name("query")
        search_box.clear()
        search_box.send_keys(actor_name)

        time.sleep(2)
        list_item = self.driver.find_element_by_css_selector("ul#search_v4_listbox > li")

        list_item.click()

        return SearchResult(self.driver)

    def navigate_to_login_page(self):
        log = self.driver.find_element_by_css_selector("a[href='/login']")
        log.click()

        return LoginPage(self.driver)



class MoviePage:
    def __init__(self, driver):
        self.driver = driver
        assert self.driver.find_element_by_css_selector("section.movie_content") is not None, "Expected to be on movie page"

    def get_movie_details(self):
        title_element = self.driver.find_element_by_css_selector("div.title > span > a > h2")
        movie_link_element = self.driver.find_element_by_css_selector("div.title > span > a")
        movie_description_element = self.driver.find_element_by_css_selector("div.overview > p")
        return title_element.text, movie_description_element.text, movie_link_element.get_attribute("href")



    def print_movie_name(self):
        title_element = self.driver.find_element_by_css_selector("div.title > span > a > h2")
        return title_element.text

    def rate_movie(self, movie_rating):
# jezeliocena jest 0 kliknij w minusw pzeciwnym wypadku kliknij w odpowiednia gwiazdke

        rate_movie = self.driver.find_element_by_css_selector("span.glyphicons.glyphicons-star.x1")
        rate_movie.click()
        rate_movie_stars = self.driver.find_elements_by_css_selector("div.rating > span.empty-stars > span.star > i")

        # if rate_movie_stars is 0
        #     rate_movie_stars.click("-")
        # else: rate_movie_stars is 0 > 1
        #     rate_movie_stars.click("1")

        actions = ActionChains(self.driver)
        actions.move_to_element_with_offset(rate_movie_stars[movie_rating - 1], 17, 0)
        actions.click()
        actions.perform()


class LoginPage:
    def __init__(self, driver):
        self.driver = driver
        assert "Login" in self.driver.title, "Expected to be on login page"

    def fill_in_credentials(self, username, password):
        user_name = self.driver.find_element_by_css_selector("input#username")
        user_name.clear()
        user_name.send_keys(username)

        password_field = self.driver.find_element_by_css_selector("input#password")
        password_field.clear()
        password_field.send_keys(password)

    def click_login_button(self):
        zaloguj = self.driver.find_element_by_css_selector("input.right.center")
        zaloguj.click()
        return ProfilePage(self.driver)


class ProfilePage:
    def __init__(self, driver):
        self.driver = driver
        assert "My Profile" in self.driver.title,  "Expected to be username on profile page"

    def search_for_movie(self, movie_title):
        search_field = self.driver.find_element_by_css_selector("input#search_v4.k-input")
        search_field.click()
        search_field.clear()
        search_field.send_keys(movie_title)

        time.sleep(2)

        list_item = self.driver.find_element_by_css_selector("ul#search_v4_listbox.k-list.k-reset >li")
        list_item.click()
        return SearchResult(self.driver)


class ActorPage:
    def __init__(self, driver):
        self.driver = driver
        assert self.driver.find_element_by_css_selector("div.person_page") is not None

    def choose_movies(self):
        movie_button = self.driver.find_element_by_css_selector('div.credits_list > h3 > span > a[media="movie"]')
        movie_button.click()
        time.sleep(2)

    def choose_random_movie(self):
        movies_list = self.driver.find_elements_by_css_selector("table.credit_group > tbody > tr > td.role > a")

        min_index = 0
        max_index = len(movies_list) - 1
        random_index = random.randrange(min_index, max_index)
        random_movie = movies_list[random_index]
        random_movie.click()
        time.sleep(5)

        return MoviePage(self.driver)


class SearchResult:
    def __init__(self, driver):
        self.driver = driver
        assert self.driver.find_element_by_css_selector("div.search_results") is not None

    def navigate_to_first_movie(self):
        actor_list = self.driver.find_element_by_css_selector("div.results > div.item > div.info > div > div > a")
        actor_list.click()
        return MoviePage(self.driver)

    def navigate_to_first_actor(self):
        actor_list = self.driver.find_element_by_css_selector("div.results > div.profile > div.content > p.name > a")
        actor_list.click()

        return ActorPage(self.driver)
