from webpage.page_objects import MainPage
from selenium import webdriver

def rate_movie(movie_title, rating, username, password):

    driver = webdriver.Firefox()
    driver.get("https://www.themoviedb.org")
    assert "TMDb" in driver.title

    mainPage = MainPage(driver)
    loginPage = mainPage.navigate_to_login_page()

    loginPage.fill_in_credentials(username, password)
    profilePage = loginPage.click_login_button()

    searchResults = profilePage.search_for_movie(movie_title)
    moviePage = searchResults.navigate_to_first_movie()
    moviePage.rate_movie(rating)

