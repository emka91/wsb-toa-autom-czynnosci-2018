from webpage.page_objects import MainPage
from selenium import webdriver
from mail_sender import send_email

def email_random_movie(actor_name, email):

    driver = webdriver.Firefox()
    driver.get("https://www.themoviedb.org")
    assert "TMDb" in driver.title

    mainPage = MainPage(driver)
    mainPage.click_cookie_notice()
    searchResults = mainPage.search_for_actor(actor_name)
    actorPage = searchResults.navigate_to_first_actor()
    actorPage.choose_movies()
    moviePage = actorPage.choose_random_movie()

    movie_title, movie_description, movie_link = moviePage.get_movie_details()
    send_email(email, movie_title, movie_description, movie_link)