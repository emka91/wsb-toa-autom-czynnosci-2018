import smtplib
from email.mime.text import MIMEText


def send_email(recepient, movie_title, movie_description, movie_link):
    email_template = """<html>
	<head>
		<style>
			body {background-color: azure; font-family: 'Lucida Sans Unicode'}
		</style>
	</head>
	<body>
		<a href='""" + movie_link + """'>""" + movie_title + """</a>
		<p>""" + movie_description + """</p>
	</body>
</html>"""
    msg = MIMEText(email_template, "html")
    msg['Subject'] = "Random movie"
    msg['From'] = "wsb.praca.dyplomowa@wp.pl"
    msg['To'] = recepient

    server = smtplib.SMTP("smtp.wp.pl", 587)
    server.ehlo()
    server.starttls()
    server.login("wsb.praca.dyplomowa", "praca.dyplomowa")
    server.sendmail("wsb.praca.dyplomowa@wp.pl", recepient, msg.as_string())

    # smtp = smtplib.SMTP_SSL('poczta.interia.pl', 465)
    # smtp.ehlo()
    # smtp.login('automato@poczta.fm', '')
    # smtp.sendmail(message['From'], message['To'], message.as_string())
    # smtp.quit()
