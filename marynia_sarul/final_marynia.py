import sys
from webpage.rate_movie import rate_movie
from webapi.rate_movie import rate_movie_by_title
from webpage.random_movie import email_random_movie
from webapi.random_movie import send_random_movie

functionality = sys.argv[1]

if functionality == "rate_movie_ui":
    movie_title = sys.argv[2]
    rating = int(sys.argv[3])
    username = sys.argv[4]
    password = sys.argv[5]

    rate_movie(movie_title, rating, username, password)

elif functionality == "rate_movie_api":
    movie_title = sys.argv[2]
    rating = int(sys.argv[3])
    session_id = sys.argv[4]

    rate_movie_by_title(movie_title, rating, session_id)

elif functionality == "random_movie_ui":
    actor_name = sys.argv[2]
    email = sys.argv[3]

    email_random_movie(actor_name, email)

elif functionality == "random_movie_api":
    actor_name = sys.argv[2]
    email = sys.argv[3]

    send_random_movie(actor_name, email)

else:
    print("Nieznane parametry")
