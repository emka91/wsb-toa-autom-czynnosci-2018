from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait

from projektAS.funkcje import AllegroPage
from projektAS.config import driver

# otwarcie przeglądarki i przejście na stronę:
driver.get("https://allegro.pl.allegrosandbox.pl/")

# maksymalizacja otwartego okna przeglądarki:
driver.maximize_window()

driver.implicitly_wait(2)
wait = WebDriverWait(driver, 5)
action_chains = ActionChains(driver)

# zamknij popup RODO:
AllegroPage.close_rodo_f(driver)
# wyszukanie oferty koszulki puma:
AllegroPage.search_f(driver)
# wykonanie screenshota z wynikami wyszukiwania i wejście w ofertę:
AllegroPage.search_details_f(driver)
# kliknięcie przycisku "kup teraz":
AllegroPage.buy_now_f(driver)
# wypełnienie danych formularza z zamówieniem:
AllegroPage.insert_data_f(driver)
# sprawdzenie czy nie jest zaznaczona opcja z chęcią zakładania nowego konta:
AllegroPage.no_new_account_assert_f(driver)
# Wykonanie screenshota końcowego z okienkiem captcha:
AllegroPage.final_shot_f(driver)

driver.close()
