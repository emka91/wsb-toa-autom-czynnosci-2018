from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait

from projektAS.funkcje import AllegroPage
from projektAS.config import driver

# otwarcie przeglądarki i przejście na stronę:
driver.get("https://allegro.pl.allegrosandbox.pl/")

# maksymalizacja otwartego okna przeglądarki:
driver.maximize_window()

driver.implicitly_wait(2)
wait = WebDriverWait(driver, 5)
action_chains = ActionChains(driver)

# zamknij popup RODO:
AllegroPage.close_rodo_f(driver)
## LOGOWANIE na stronie głównej:
AllegroPage.click_login_f(driver)
# wprowadzenie danych - login:
AllegroPage.insert_login_f(driver)
# wprowadzenie danych - hasło:
AllegroPage.insert_password_f(driver)
# logowanie po wprowadzeniu danych:
# ustawienie widoku na pasku "zaloguj się", aby losowo generowany (nie zawsze jest) popup nie przysłonił przycisku:
AllegroPage.login_f(driver, wait)
## czekanie aż strona się załaduje (pojawienie się loginu fajnitesterzy3 w panelu)
AllegroPage.wait_txt_login_f(driver, wait)
## WYSTAWIENIE PRZEDMIOTU NA SPRZEDAŻ
# otwarcie panelu po logowaniu:
AllegroPage.my_allegro_f(driver)
# kliknięcie "wystaw przedmiot":
AllegroPage.add_item_f(driver)
# ustawienie widoku na pasku "wystaw przedmiot":
AllegroPage.scroll_location_wystaw_przedmiot_f(driver)
# wprowadzenie tytułu oferty:
AllegroPage.offer_title_f(driver)
# wybór kategorii przedmiotu:
AllegroPage.category_f(driver)
# wybór stanu przedmiotu jako nowy:
AllegroPage.state_f(driver)
# wybór rozmiaru przedmiotu:
AllegroPage.size_f(driver)
# wybór koloru przedmiotu:
AllegroPage.color_f(driver)
# wybór marki przedmiotu:
AllegroPage.mark_f(driver)
# wybór wzoru dominującego:
AllegroPage.pattern_f(driver)
# wybór większej ilości parametrów:
AllegroPage.more_parameters_f(driver)
# wybór płci:
AllegroPage.sex_choose_f(driver)
# dodanie opisu wystawianego przedmiotu:
AllegroPage.add_comment_f(driver, action_chains)
# sprawdzenie czy opcja "kup teraz" jest domyślnie znaznaczona:
AllegroPage.auction_checkbox_assert_f(driver)
# ustawienie ceny przedmiodu:
AllegroPage.price_f(driver)
# określenie ilości dostępnych sztuk:
AllegroPage.quantity_f(driver)
# oznaczenie ilości jako sztuki:
AllegroPage.quantity_type_f(driver)
# określenie czasu trwania aukcji:
AllegroPage.duration_f(driver)
# wybór opcji faktury vat:
AllegroPage.vat_option_f(driver)
# dodanie cennika dostawy kurierem:
AllegroPage.delivery_f(driver)
# przewidywany czas wysyłki towaru:
AllegroPage.shipping_time_f(driver)
# dodanie dodatkowych informacji o przesyłce:
AllegroPage.add_extra_info_f(driver)
# wybór opcji promowania poprzez wyróżnienie:
AllegroPage.promo_f(driver)
# wybór daty wystawienia aukcji:
AllegroPage.scroll_add_date_f(driver)
# sprawdzenie czy opcja wystawienia "od razu" jest zaznaczona:
AllegroPage.add_now_assert_f(driver)
# ustawienie widoku na podsumowanie:
AllegroPage.summary_card_f(driver)
## ZOBACZENIE PODGLĄDU WPROWADZONEJ OFERTY I WYKONANIE SCREENSHOTA OFERTY:
AllegroPage.show_preview_f(driver)
# wystawienie przedmiotu, potwierdzenie wystawienia (asercja) i wykonanie screenshota:
AllegroPage.post_auction_assert_f(driver, wait)
# wylogowanie po wystawieniu przedmiotu:
AllegroPage.logout_f(driver)
# Wykonanie screenshota końcowego z wykonanego i zakończonego skryptu:
AllegroPage.end_f(driver)

driver.close()
