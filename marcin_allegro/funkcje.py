from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions

from projektAS.config import PROJECT_ROOT

class AllegroPage():

    ####### LOGIN - POST - AUCTION #########

    # zamknij popup RODO:
    def close_rodo_f(driver):
        close_rodo = driver.find_element_by_css_selector("._8tsq7")
        close_rodo.click()

    # otwarcie panelu do logowania:
    def click_login_f(driver):
        my_allegro = driver.find_element_by_css_selector("button._iu5pr")
        my_allegro.click()
        driver.implicitly_wait(2)
        my_allegro_login = driver.find_element_by_css_selector("a._z41ha:nth-child(2)")
        my_allegro_login.click()

    # wprowadzenie danych - login:
    def insert_login_f(driver):
        insert_login = driver.find_element_by_id("username")
        insert_login.click()
        insert_login.send_keys("fajnitesterzy3@gmail.com")

    # wprowadzenie danych - hasło:
    def insert_password_f(driver):
        insert_password = driver.find_element_by_id("password")
        insert_password.click()
        insert_password.send_keys("Poi098098")
        driver.implicitly_wait(2)

    # logowanie po wprowadzeniu danych:
    # ustawienie widoku na pasku "zaloguj się", aby losowo generowany (nie zawsze jest) popup nie przysłonił przycisku:
    def login_f(driver, wait):
        scroll_location_login = driver.find_element_by_class_name("ng-scope")
        scroll_location_login.location_once_scrolled_into_view
        login_button = driver.find_element_by_id("login-button")
        wait.until(expected_conditions.element_to_be_clickable((By.ID, "login-button")))
        login_button.click()

    ## czekanie aż strona się załaduje (pojawienie się loginu fajnitesterzy3 w panelu)
    def wait_txt_login_f(driver, wait):
        driver.implicitly_wait(2)
        wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, "button._iu5pr"), "fajnitesterzy3"))

    ## WYSTAWIENIE PRZEDMIOTU NA SPRZEDAŻ
    # otwarcie panelu po logowaniu:
    def my_allegro_f(driver):
        my_allegro = driver.find_element_by_css_selector("button._iu5pr")
        my_allegro.click()

    # kliknięcie "wystaw przedmiot":
    def add_item_f(driver):
        add_item = driver.find_element_by_css_selector("a.fee54_29_-w:nth-child(1)")
        add_item.click()

    # ustawienie widoku na pasku "wystaw przedmiot"
    def scroll_location_wystaw_przedmiot_f(driver):
        scroll_location_wystaw_przedmiot = driver.find_element_by_css_selector("._mbz3k")
        scroll_location_wystaw_przedmiot.location_once_scrolled_into_view

    # wprowadzenie tytułu oferty:
    def offer_title_f(driver):
        offer_title = driver.find_element_by_css_selector("#name")
        offer_title.click()
        offer_title.send_keys("Puma - damska koszulka XS - SUPEROFERTA!")
        driver.implicitly_wait(2)

    # wybór kategorii przedmiotu:
    def category_f(driver):
        select_category = driver.find_element_by_id("select-category")
        select_category.click()
        driver.implicitly_wait(2)
        more_categories = driver.find_element_by_css_selector(".margin-left-28")
        more_categories.click()
        driver.implicitly_wait(2)
        choose_category = driver.find_element_by_tag_name("span._n2w3y")
        choose_category.click()

    # wybór stanu przedmiotu jako nowy:
    def state_f(driver):
        scroll_location_wybor_stanu = driver.find_element_by_css_selector("#attributes-card > h2:nth-child(1)")
        scroll_location_wybor_stanu.location_once_scrolled_into_view
        state_choose = driver.find_element_by_id("11323")
        state_choose.click()
        new_state = driver.find_element_by_xpath('//option[@value="Nowy"]')
        new_state.click()

    # wybór rozmiaru przedmiotu:
    def size_f(driver):
        size_choose = driver.find_element_by_id("3806")
        size_choose.click()
        xs_s_size = driver.find_element_by_xpath('//option[@value="XS/S"]')
        xs_s_size.click()

    # wybór koloru przedmiotu:
    def color_f(driver):
        color_choose = driver.find_element_by_id("Kolor_Odcienie fioletu")
        color_choose.click()

    # wybór marki przedmiotu:
    def mark_f(driver):
        mark_choose = driver.find_element_by_id("3786")
        mark_choose.click()
        mark_puma = driver.find_element_by_xpath('//option[@value="Puma"]')
        mark_puma.click()

    # wybór wzoru dominującego:
    def pattern_f(driver):
        pattern_choose = driver.find_element_by_id("3766")
        pattern_choose.click()
        without_pattern = driver.find_element_by_xpath('//option[@value="bez wzoru"]')
        without_pattern.click()

    # wybór większej ilości parametrów:
    def more_parameters_f(driver):
        more_parameters = driver.find_element_by_css_selector("button._vrdqo:nth-child(2)")
        more_parameters.location_once_scrolled_into_view
        more_parameters.click()

    # wybór płci:
    def sex_choose_f(driver):
        sex_choose = driver.find_element_by_id("15851")
        sex_choose.click()
        sex_female = driver.find_element_by_xpath('//option[@value="Produkt damski"]')
        sex_female.click()

    # dodanie opisu wystawianego przedmiotu:
    def add_comment_f(driver, action_chains):
        scroll_location_add_comment = driver.find_element_by_css_selector(
            "#description-card > div:nth-child(3) > h3:nth-child(1)")
        scroll_location_add_comment.location_once_scrolled_into_view
        text_field = driver.find_element_by_id("mceu_38")
        # action_chains z dodaniem opisu:
        action_chains.click(text_field)
        action_chains.send_keys("Witamy na naszej aukcji!")
        action_chains.send_keys(Keys.ENTER)
        action_chains.send_keys("Tylko teraz superpromocja. Zamów już dziś oryginalną koszulkę firmy Puma.")
        action_chains.send_keys(Keys.ENTER)
        action_chains.send_keys("Życzymy udanych zakupów!")
        action_chains.perform()

    # sprawdzenie czy opcja "kup teraz" jest domyślnie znaznaczona:
    def auction_checkbox_assert_f(driver):
        auction_checkbox = driver.find_element_by_id("checkbox-1")
        buy_now_checkbox = driver.find_element_by_id("checkbox-2")
        assert not auction_checkbox.is_selected()
        print("Licytuj - nie jest zaznaczone - OK")
        assert buy_now_checkbox.is_selected()
        print("Kup teraz - jest zaznaczone - OK")

    # ustawienie ceny przedmiodu:
    def price_f(driver):
        price_input = driver.find_element_by_id("buynow-price")
        price_input.click()
        price_input.send_keys("19.99")

    # określenie ilości dostępnych sztuk:
    def quantity_f(driver):
        quantity = driver.find_element_by_id("quantity")
        quantity.click()
        quantity.send_keys("5")

    # oznaczenie ilości jako sztuki:
    def quantity_type_f(driver):
        quantity_type = driver.find_element_by_css_selector(
            "div._t0xzz:nth-child(2) > div:nth-child(1) > div:nth-child(1) > select:nth-child(1)")
        quantity_type.click()
        quantity_type_pieces = driver.find_element_by_xpath('//option[@value="UNIT"]')
        quantity_type_pieces.click()

    # określenie czasu trwania aukcji:
    def duration_f(driver):
        duration = driver.find_element_by_css_selector(
            "#terms-card > div:nth-child(4) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > label:nth-child(2)")
        duration.click()

    # wybór opcji faktury vat:
    def vat_option_f(driver):
        vat_option = driver.find_element_by_id("invoice")
        vat_option.click()
        vat_choose = driver.find_element_by_xpath('//option[@value="VAT"]')
        vat_choose.click()

    # dodanie cennika dostawy kurierem:
    def delivery_f(driver):
        delivery_methods_button = driver.find_element_by_id("delivery-methods-modal-trigger")
        delivery_methods_button.click()
        delivery_courier_1 = driver.find_element_by_css_selector(
            "div.col-sm-10:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > label:nth-child(2)")
        delivery_courier_1.click()
        delivery_courier_1_price = driver.find_element_by_css_selector(".measurement")
        delivery_courier_1_price.click()
        delivery_courier_1_price.send_keys("0")
        delivery_courier_1_max = driver.find_element_by_css_selector(
            "div.col-sm-4:nth-child(2) > div:nth-child(2) > input:nth-child(1)")
        delivery_courier_1_max.clear()
        delivery_courier_1_max.send_keys("2")
        delivery_courier_1_next_price = driver.find_element_by_css_selector(
            "div.col-sm-4:nth-child(3) > div:nth-child(2) > input:nth-child(1)")
        delivery_courier_1_next_price.clear()
        delivery_courier_1_next_price.send_keys("0,00")
        delivery_courier_save_button = driver.find_element_by_css_selector(".btn-primary")
        delivery_courier_save_button.click()

    # przewidywany czas wysyłki towaru:
    def shipping_time_f(driver):
        estimated_shipping = driver.find_element_by_id("estimatedShippingTimeId")
        estimated_shipping.click()
        shipping_24h = driver.find_element_by_xpath('//option[@value="PT24H"]')
        shipping_24h.click()

    # dodanie dodatkowych informacji o przesyłce:
    def add_extra_info_f(driver):
        add_extra_info_button = driver.find_element_by_css_selector(
            "#delivery-card > div:nth-child(5) > div:nth-child(2) > div:nth-child(2) > button:nth-child(1)")
        add_extra_info_button.click()
        add_extra_info_box = driver.find_element_by_id("delivery-additional-info")
        add_extra_info_box.click()
        add_extra_info_box.send_keys("Darmowa wysyłka kurierem!")
        add_extra_info_save_button = driver.find_element_by_css_selector(".btn-primary")
        add_extra_info_save_button.click()

    # wybór opcji promowania poprzez wyróżnienie:
    def promo_f(driver):
        promo_option = driver.find_element_by_css_selector(
            "div.col-xm-12:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > label:nth-child(2)")
        promo_option.click()

    # wybór daty wystawienia aukcji:
    def scroll_add_date_f(driver):
        scroll_location_add_date = driver.find_element_by_css_selector("#publication-card > h2:nth-child(1)")
        scroll_location_add_date.location_once_scrolled_into_view

    # sprawdzenie czy opcja wystawienia "od razu" jest zaznaczona:
    def add_now_assert_f(driver):
        add_now = driver.find_element_by_id("publicationTimeNow")
        assert add_now.is_selected()
        print("Od razu - jest zaznaczone - OK")

    # ustawienie widoku na podsumowanie:
    def summary_card_f(driver):
        summary_card = driver.find_element_by_css_selector("#summary-card > h2:nth-child(1)")
        summary_card.location_once_scrolled_into_view

    ## ZOBACZENIE PODGLĄDU WPROWADZONEJ OFERTY I WYKONANIE SCREENSHOTA OFERTY:
    def show_preview_f(driver):
        show_prewiev_offer = driver.find_element_by_css_selector("div._n1rmb:nth-child(1) > button:nth-child(1)")
        show_prewiev_offer.click()
        # uśpienie skryptu do załadowania strony, aby wykonać screenshota:
        sleep(4)
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(1)_podgląd_oferty.png")
        back_to_offer = driver.find_element_by_css_selector(".btn-default")
        back_to_offer.click()
        # sleep(3)

    # wystawienie przedmiotu, potwierdzenie wystawienia (asercja) i wykonanie screenshota:
    def post_auction_assert_f(driver, wait):
        confirm_button = driver.find_element_by_id("form-button-confirm")
        confirm_button.click()
        # odświeżenie strony, ponieważ nie wczytyało potwierdzenia:
        sleep(3)
        # driver.refresh()
        # potwierdzenie:
        driver.implicitly_wait(2)
        wait.until(expected_conditions.text_to_be_present_in_element(
            (By.CSS_SELECTOR, "div._1bjbu:nth-child(2) > div:nth-child(1) > h2:nth-child(1)"),
            "Wystawiłeś ofertę w serwisie"))
        assert (expected_conditions.text_to_be_present_in_element(
            (By.CSS_SELECTOR, "div._1bjbu:nth-child(2) > div:nth-child(1) > h2:nth-child(1)"),
            "Wystawiłeś ofertę w serwisie"))
        print("Przedmiot wystawiony pomyślnie - OK")
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(2)_przedmiot_wystawiony.png")

    # wylogowanie po wystawieniu przedmiotu:
    def logout_f(driver):
        my_allegro = driver.find_element_by_css_selector("button._iu5pr")
        my_allegro.click()
        driver.implicitly_wait(2)
        my_allegro_logout = driver.find_element_by_css_selector("a._z41ha")
        my_allegro_logout.click()
        driver.implicitly_wait(5)

    # Wykonanie screenshota końcowego z wykonanego i zakończonego skryptu:
    def end_f(driver):
        driver.implicitly_wait(8)
        sleep(3)
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(3)_koniec_logout.png")
        print("KONIEC SKRYPTU - OK")

    ####### SEARCH - AND - BUY #########

    # wyszukanie oferty koszulki puma:
    def search_f(driver):
        search_field = driver.find_element_by_css_selector(".metrum-search__query")
        search_field.click()
        search_field.send_keys("puma")
        search_submit = driver.find_element_by_css_selector(".metrum-search__submit")
        search_submit.click()
        driver.implicitly_wait(2)

    # wykonanie screenshota z wynikami wyszukiwania i wejście w ofertę:
    def search_details_f(driver):
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(4)_wynik_wyszukiwania.png")
        show_details = driver.find_element_by_link_text("Puma - damska koszulka XS - SUPEROFERTA!")
        show_details.click()

    # kliknięcie przycisku "kup teraz":
    def buy_now_f(driver):
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(5)_szczegóły_oferty.png")
        buy_now = driver.find_element_by_id("buy-now")
        buy_now.click()
        login_button_guest = driver.find_element_by_id("login-button-guest")
        login_button_guest.click()

    # wypełnienie danych formularza z zamówieniem:
    def insert_data_f(driver):
        email_input = driver.find_element_by_id("email-input")
        email_input.click()
        email_input.send_keys("johnny.kowalski@domena.pl")
        firstname_input = driver.find_element_by_id("firstname-input")
        firstname_input.click()
        firstname_input.send_keys("Jan")
        lastname_input = driver.find_element_by_id("lastname-input")
        lastname_input.click()
        lastname_input.send_keys("Kowalski")
        street_input = driver.find_element_by_id("street-input")
        street_input.click()
        street_input.send_keys("ul. Kupieca 6/9")
        postalcode_input = driver.find_element_by_id("postalcode-input")
        postalcode_input.click()
        postalcode_input.send_keys("22-976")
        city_input = driver.find_element_by_css_selector(".col-xm-8 > input:nth-child(1)")
        city_input.click()
        city_input.send_keys("Warszawa")
        phone_input = driver.find_element_by_id("phone-input")
        phone_input.click()
        phone_input.send_keys("700700700")
        captcha = driver.find_element_by_id("guest-captcha")
        captcha.click()
        captcha.location_once_scrolled_into_view

    # sprawdzenie czy nie jest zaznaczona opcja z chęcią zakładania nowego konta:
    def no_new_account_assert_f(driver):
        assert not driver.find_element_by_css_selector(".checkbox > label:nth-child(1) > input:nth-child(1)").is_selected()
        print("Nie chcę zakładać nowego konta podczas zakupu - OK")

    # Wykonanie screenshota końcowego z okienkiem captcha:
    def final_shot_f(driver):
        sleep(2)
        driver.save_screenshot(PROJECT_ROOT + "/../screenshots" + "/[allegro]_(6)_koniec_CAPTCHA.png")
        print("KONIEC SKRYPTU - OK")
        sleep(3)
