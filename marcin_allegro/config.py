import os
from selenium import webdriver

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
GECKO_DRIVER_EXECUTABLE = PROJECT_ROOT + "/../selenium-drivers/firefox-driver/geckodriver.exe"
FIREFOX_BINARY = None

driver = webdriver.Firefox(executable_path=GECKO_DRIVER_EXECUTABLE)
