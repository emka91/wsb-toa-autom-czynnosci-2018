import os

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# Firefox

GECKO_DRIVER_EXECUTABLE = PROJECT_ROOT + "/../../env/selenium-drivers/geckodriver.exe"
FIREFOX_BINARY = PROJECT_ROOT + "/../../env/portable-browsers/FirefoxPortable/App/Firefox64/firefox.exe"