import json

import requests

import config


class Rest():
    def __init__(self):
        self.request = {"isFlightChange": 'false', "isSeniorOrStudent": 'false',
                        "flightList": [
                            {"departureStation": config.departure_station, "arrivalStation": config.arrival_station,
                             "departureDate": config.departure_date},
                            {"departureStation": config.arrival_station, "arrivalStation": config.departure_station,
                             "departureDate": config.return_date}],
                        "adultCount": config.adult_count, "childCount": 0, "infantCount": 0,
                        "wdc": 'true'}
        self.response = {}

    def send_request(self):
        r = requests.post("https://be.wizzair.com/7.14.1/Api/search/search", json=self.request)
        self.response = json.loads(r.text)

    def get_flight_price(self):
        return self.response['outboundFlights'][0]['fares'][3]['basePrice']['amount']

    def get_return_price(self):
        return self.response['returnFlights'][0]['fares'][3]['basePrice']['amount']
