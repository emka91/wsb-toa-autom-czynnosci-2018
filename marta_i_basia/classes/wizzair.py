import datetime
from time import strftime, sleep

from selenium import webdriver

import selenium_config


class WizzairPage():

    def __init__(self):
        self.driver = webdriver.Firefox(
            executable_path=selenium_config.GECKO_DRIVER_EXECUTABLE,
            firefox_binary=selenium_config.FIREFOX_BINARY
        )
        self.driver.implicitly_wait(2)
        self.driver.get("https://wizzair.com/pl-pl#/")
        self.driver.maximize_window()

    def enter_departure_city_name(self, city_name):
        self.driver.find_element_by_id("search-departure-station").send_keys(city_name)
        self.driver.find_element_by_tag_name("mark").click()

    def enter_arrival_city_name(self, city_name):
        self.driver.find_element_by_id("search-arrival-station").send_keys(city_name)
        self.driver.find_element_by_tag_name("mark").click()

    def select_dates(self, departure_date, return_date):
        sleep(2)
        actual_month = int(strftime('%m'))
        dep_date = datetime.datetime.strptime(departure_date, "%Y-%m-%d")
        ret_date = datetime.datetime.strptime(return_date, "%Y-%m-%d")

        while actual_month < dep_date.month:
            self.driver.find_element_by_class_name("pika-next").click()
            actual_month = actual_month + 1
        self.driver.find_element_by_css_selector('button[data-pika-day="'+str(dep_date.day)+'"]').click()

        while actual_month < ret_date.month:
            self.driver.find_element_by_class_name("pika-next").click()
            actual_month = actual_month + 1

        self.driver.find_element_by_css_selector('button[data-pika-day="'+str(ret_date.day)+'"]').click()

    def enter_adults_passengers_amount(self, amount):
        self.driver.find_element_by_id("search-passenger").click()
        actual_amount = int(self.driver.find_element_by_css_selector(
            '.flight-search__panel--sub--people input.stepper__content__number').get_attribute('value'))
        while actual_amount < amount:
            self.driver.find_element_by_css_selector(".flight-search__panel--sub--people .stepper__button--add").click()
            actual_amount += 1

    def flight_search(self):
        self.driver.find_element_by_css_selector(".flight-search__panel__cta-btn").click()
        windows = self.driver.window_handles
        self.driver.switch_to.window(windows[1])

    def get_flight_price(self):
        sel = "#fare-selector-outbound li.js-selected span.booking-flow__flight-select__chart__day__price"
        flight_price_with_currency = self.driver.find_element_by_css_selector(sel).text.split(" ")
        return float(flight_price_with_currency[0].replace(',', '.'))

    def get_return_flight_price(self):
        sel = "#fare-selector-return li.js-selected span.booking-flow__flight-select__chart__day__price"
        return_flight_price_with_currency = self.driver.find_element_by_css_selector(sel).text.split(" ")
        return float(return_flight_price_with_currency[0].replace(",", "."))

    def quit(self):
        self.driver.quit()
