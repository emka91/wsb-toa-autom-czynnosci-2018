import smtplib
from email.mime.text import MIMEText

import config


class Email():

    def __init__(self):
        self.mail_from = 'Automato <abc.bc@wp.pl>'
        self.mail_to = 'martagajewska19@wp.pl'
        self.mail_subject = "Lot z " + config.departure_station + ' do ' + config.arrival_station
        self.login = 'abc.bc@wp.pl'
        self.password = 'abc12345'

    def send_email_about_flight_price(self, selenium_flight_price, selenium_return_flight_price, rest_flight_price,
                                      rest_return_flight_price):
        msg = "Lot z " + config.departure_station + ' do ' + config.arrival_station + '  w terminie ' + config.departure_date + '-' + config.return_date + ' <br/>\n\
               Informacje z selenium: Cena wylotu: ' + str(selenium_flight_price) + ' zł. Cena powrotu: ' + str(selenium_return_flight_price) + ' zł. <br/>\n \
               Informacje z rest: Cena wylotu: ' + str(rest_flight_price) + ' zł. Cena powrotu: ' + str(rest_return_flight_price) + ' zł. <br/>\n'
        message = MIMEText(msg, 'html')
        message['To'] = self.mail_to
        message['From'] = self.mail_from
        message['Subject'] = self.mail_subject
        self.smtp = smtplib.SMTP_SSL('smtp.wp.pl', 465)
        self.smtp.ehlo()
        self.smtp.login(self.login, self.password)
        self.smtp.sendmail(self.mail_from, self.mail_to, message.as_string())
        self.smtp.quit()
