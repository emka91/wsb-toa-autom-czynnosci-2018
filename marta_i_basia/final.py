import config
from classes.email import Email
from classes.rest import Rest
from classes.wizzair import WizzairPage

wizz = WizzairPage()

wizz.enter_departure_city_name(config.departure_station)
wizz.enter_arrival_city_name(config.arrival_station)
wizz.select_dates(config.departure_date,config.return_date)
wizz.enter_adults_passengers_amount(config.adult_count)
wizz.flight_search()
selenium_flight_price = wizz.get_flight_price()
selenium_return_flight_price = wizz.get_return_flight_price()

print("Cena lotu to: " + str(selenium_flight_price))
print("Cena powrotu to: " + str(selenium_return_flight_price))

wizz.quit()


rest = Rest()

rest.send_request()
rest_flight_price = rest.get_flight_price()
rest_return_price = rest.get_return_price()


print("Cena lotu to: " + str(rest_flight_price))
print("Cena powrotu to: " + str(rest_return_price))


if selenium_flight_price + selenium_return_flight_price < config.price_notify:
    email = Email()
    email.send_email_about_flight_price(selenium_flight_price, selenium_return_flight_price,rest_flight_price,rest_return_price)