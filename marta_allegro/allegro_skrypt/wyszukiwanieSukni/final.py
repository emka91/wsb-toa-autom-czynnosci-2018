from zeep import Client
from wyszukiwanieSukni.emailWysylka import Email
from wyszukiwanieSukni.config import OfferDetails as OD
from wyszukiwanieSukni import config
from wyszukiwanieSukni import funkcje as FN

client = Client(config.url)

# wywołanie metody doQueryAllSysStatus, służącej do pobierania informacji o aktualnych wersjach komponentów Allegro WebApi
response = client.service.doQueryAllSysStatus(config.countryId, config.myApiKey)
verKey = response[0].verKey

# logowanie metodą doLogin
auth = client.service.doLogin(config.userLogin, config.userPassword, config.countryId, config.myApiKey, verKey)
sessionId = auth.sessionHandlePart
print("Logowanie poprawne.")

# wyszukiwanie sukni
result = client.service.doGetItemsList(webapiKey=config.myApiKey, filterOptions=config.tablica,
                                      countryId=config.countryId)
numberOfResults = str(result.itemsCount)

# tablica do której wpadają numery ofert i ceny
offers = []
x = 0
while True:
    oferta = OD()
    oferta.number = result.itemsList.item[x].itemId
    oferta.price = result.itemsList.item[x].priceInfo.item[x].priceValue
    offers.append(oferta)

    x = x + 1
    if x == int(numberOfResults):
        break

offerStr = FN.viewOffers(offers)

# wysyłanie mejla w zależności od wyników wyszukiwania
if numberOfResults > "0":
    email = Email()
    email.sendEmailWithOffers(offerStr, len(offers))
    print('Wysłano wiadomość email na Twój adres. Sprawdź skrzynkę')
else:
    email = Email()
    email.sendEmailWithoutOffers(len(offers))
    print('Nie znaleziono interesującej Cię oferty.')
