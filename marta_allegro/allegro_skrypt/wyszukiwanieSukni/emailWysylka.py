import smtplib
from email.mime.text import MIMEText


class Email():

    def __init__(self):
        self.mail_from = 'Automato <testerkimbm@wp.pl>'
        self.mail_to = 'fajnitesterzy3@gmail.com'
        self.mail_subject = 'Suknie wieczorowe - wynik wyszukiwania'
        self.login = 'testerkimbm@wp.pl'
        self.password = 'studia2018'
        self.mail_subject_new_offer = "Została utworzona nowa oferta"
    # Utworzenie wiadomości:

    def sendEmailWithOffers(self, offersStr, numberOfResult):
        message_text = "Znaleziono " + str(numberOfResult) + " wyszukań.<br />" + offersStr
        message = MIMEText(message_text, 'html')
        message['From'] = self.mail_from
        message['To'] = self.mail_to
        message['Subject'] = self.mail_subject

        # Wyslanie wiadomosci przez serwer SMTP
        smtp = smtplib.SMTP_SSL('smtp.wp.pl', 465)
        smtp.ehlo()
        smtp.login(self.login, self.password)
        smtp.sendmail(self.mail_from, self.mail_to, message.as_string())
        smtp.quit()

    def sendEmailWithoutOffers(self, numberOfResult):
        message_text = "Nie odnaleziono żadnej oferty spełniającej kryteria wyszukiwania. Liczba ofert wynosi: " + str(numberOfResult)
        message = MIMEText(message_text, 'html')
        message['From'] = self.mail_from
        message['To'] = self.mail_to
        message['Subject'] = self.mail_subject

        # Wyslanie wiadomosci przez serwer SMTP
        smtp = smtplib.SMTP_SSL('smtp.wp.pl', 465)
        smtp.ehlo()
        smtp.login(self.login, self.password)
        smtp.sendmail(self.mail_from, self.mail_to, message.as_string())
        smtp.quit()

    def sendEmailWithNewOfert(self, numberOfert, priceOffer):
        message_text = "Została utworzona nowa oferta o numerze " + numberOfert + ". Opłata za wystawienie oferty to: " + priceOffer + "."
        message = MIMEText(message_text, 'html')
        message['From'] = self.mail_from
        message['To'] = self.mail_to
        message['Subject'] = self.mail_subject_new_offer

        # Wyslanie wiadomosci przez serwer SMTP
        smtp = smtplib.SMTP_SSL('smtp.wp.pl', 465)
        smtp.ehlo()
        smtp.login(self.login, self.password)
        smtp.sendmail(self.mail_from, self.mail_to, message.as_string())
        smtp.quit()