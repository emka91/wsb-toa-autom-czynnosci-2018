import funkcje as FN
import zmienne as ZM

# pobranie verKey z metody doQueryAllSysStatus
verKey = FN.doQueryAllSysStatus(ZM.webapiKey, ZM.countryCode)
print("Uzyskano klucz wersji: ", verKey)

# pobranie sessionHandlePart (sessionId) z metody doLogin

sessionId = FN.logowanie(ZM.userLogin, ZM.userPassword, ZM.webapiKey)
print("Logowanie poprawne. Uzyskany klucz sesji to: ", sessionId)

oferta = FN.newOffer(sessionId, ZM.offerTitle, ZM.categoryNumber, ZM.duration, ZM.numberOfItem, ZM.priceBuyNow, ZM.numberCountry, ZM.numberProvince, ZM.city, ZM.shippingCost, ZM.quantity, ZM.formatShipping, ZM.automaticRenewal, ZM.postCode, ZM.courierShipmentFirstItem, ZM.prepaymentInPost, ZM.shippingDate, ZM.description, ZM.cleaverage, ZM.mainMaterial, ZM.colour, ZM.mainPattern, ZM.mark, ZM.condition, ZM.sleeveType, ZM.additionalFeatures, ZM.size, ZM.localId, ZM.impliedWarranty, ZM.returnPolicy, ZM.warranty)
print("Sukces! Twój numer oferty to: ", oferta)






