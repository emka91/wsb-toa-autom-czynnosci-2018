from zeep import Client
import oferta
from wystawienieOferty import config
from wyszukiwanieSukni.emailWysylka import Email

client = Client(config.url)

# wywołanie metody doQueryAllSysStatus, służącej do pobierania informacji o aktualnych wersjach komponentów Allegro WebApi
response = client.service.doQueryAllSysStatus(config.countryId, config.myApiKey)
verKey = response[0].verKey
print("Klucz wersji to: ", verKey)

# logowanie metodą doLogin
auth = client.service.doLogin(config.userLogin, config.userPassword, config.countryId, config.myApiKey, verKey)
sessionId = auth.sessionHandlePart
print("Logowanie poprawne. Uzyskany klucz sesji to: ", sessionId)

# wystawienie oferty
itemFields = oferta.doNewAuction(config.offerTitle, config.categoryNumber, config.duration, config.numberOfItem, config.priceBuyNow,
                                 config.numberCountry, config.numberProvince, config.city, config.shippingCost, config.quantity,
                                 config.formatShipping, config.automaticRenewal, config.postCode, config.courierShipmentFirstItem,
                                 config.prepaymentInPost, config.shippingDate, config.description, config.cleaverage, config.mainMaterial,
                                 config.colour, config.mainPattern, config.mark, config.condition, config.sleeveType, config.additionalFeatures,
                                 config.size)

myOffer = client.service.doNewAuctionExt(sessionHandle=sessionId, fields=itemFields, localId=config.localId,
                                             afterSalesServiceConditions=config.afterSalesServiceConditions)
numberOffer = myOffer.itemId
priceOffer = myOffer.itemInfo
print('Udało się wystawić nową ofertę o numerze: ' + str(numberOffer))

if numberOffer > 0:
    email = Email()
    email.sendEmailWithNewOfert(str(numberOffer), str(priceOffer))
    print('Wysłano wiadomość email na Twój adres. Sprawdź skrzynkę')
else:
    print('Nie udało się utworzyć nowej oferty. Spróbuj jeszcze raz.')
